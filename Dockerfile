FROM node

WORKDIR /app

COPY . /app

RUN yarn install

RUN yarn add sharp

EXPOSE 3000

CMD ["yarn", "run", "production"]
